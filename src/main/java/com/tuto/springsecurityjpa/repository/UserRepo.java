package com.tuto.springsecurityjpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tuto.springsecurityjpa.entity.UserEntity;

public interface UserRepo extends JpaRepository<UserEntity, Integer> {
	
	Optional <UserEntity>findByUserName(String userName);

}
